FROM ubuntu:23.10
ENV USER=cpatrut
ENV UID=12345
ENV GID=23456
RUN apt update;  apt  install adduser  curl unzip libc6 wget build-essential libz-dev zlib1g-dev   -y; apt upgrade -y
RUN adduser \
  --disabled-password \
  --gecos "" \
  --home "$(pwd)" \
  --ingroup "root" \
  --no-create-home \
  --uid "$UID" \
  "$USER"
ENV MAVEN_VERSION 3.9.4
ENV GRAALVM_VERION 22.3.2

RUN wget https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-$GRAALVM_VERION/graalvm-ce-java17-linux-amd64-$GRAALVM_VERION.tar.gz && \
  tar -zxvf graalvm-ce-java17-linux-amd64-$GRAALVM_VERION.tar.gz && \
  rm graalvm-ce-java17-linux-amd64-$GRAALVM_VERION.tar.gz  
ENV JAVA_HOME=/graalvm-ce-java17-$GRAALVM_VERION
ENV GRAALVM_HOME=/graalvm-ce-java17-$GRAALVM_VERION
ENV PATH="$JAVA_HOME/bin:$PATH"

RUN dpkg -l libc6

RUN /graalvm-ce-java17-$GRAALVM_VERION/bin/gu install native-image

RUN wget https://dlcdn.apache.org/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  tar -zxvf apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  rm apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  mv apache-maven-$MAVEN_VERSION /usr/lib/mvn
ENV PATH=/"usr/lib/mvn/bin:$PATH"


RUN mkdir  /home/cpatrut && chown cpatrut:root /home/cpatrut && mkdir /.m2 ; chown cpatrut:root /.m2/

USER cpatrut
WORKDIR /home/cpatrut

